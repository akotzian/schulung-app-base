# Voraussetzungen
* Gitlab Account

# Aufgabe 1
a) Erstelle via Gitlab CI für das schulungs-app Projekt einen automatischen Build ausführt und diesen für 2h als Artifact bereitstellt 

b) Erweitere die Pipeline um einen Job der die Tests automatisiert ausführt

# Aufgabe 2 
Erweitere die Pipeline um einen Job der ein Docker Image baut und es in die Gitlab Registry pusht